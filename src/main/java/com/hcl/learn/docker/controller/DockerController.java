package com.hcl.learn.docker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DockerController {
	
	@GetMapping("/message")
	public String getMessage() {
		return "Wellcome to the docker-page";
	}

}
